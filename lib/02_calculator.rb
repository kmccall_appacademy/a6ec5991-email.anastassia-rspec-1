#Anastassia Bobokalonova
#March 27, 2017

def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(numbers)
  numbers.reduce(0) {|acc, el| acc + el}
end

def multiply(numbers)
  numbers.reduce(:*)
end

def power(num, exponent)
  num ** exponent
end

def factorial(num)
  return 1 if num == 0 
  (1..num).each.reduce(:*)
end
