#Anastassia Bobokalonova
#March 27, 2017

def echo(phrase)
  phrase
end

def shout(phrase)
  phrase.upcase
end

def repeat(phrase, num=2)
  ([phrase] * num).join(" ")
end


def start_of_word(phrase, num)
  phrase[0...num]
end

def first_word(phrase)
  phrase.split[0]
end

def titleize(phrase)
  little_words = ["a", "the", "to", "over", "and"]
  title = phrase.split
  title.map!.each_with_index do |word, i|
    if little_words.include?(word) && i > 0
      word
    else
      word.capitalize
    end
  end
  title.join(" ")
end
