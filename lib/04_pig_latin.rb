#Anastassia Bobokalonova
#March 27, 2017

#Rule 1: If a word begins with a vowel sound, add an "ay" sound to
# the end of the word.
#
# Rule 2: If a word begins with a consonant sound, move it to the end
# of the word, and then add an "ay" sound to the end of the word.

def translate(phrase)
  pig_words = []
  phrase.split.each do |word|
    idx = find_switch_point(word)
    if idx == 0
      pig_words << word + "ay"
    else
      pig_words << word[idx..-1] + word[0...idx] + "ay"
    end
  end
  pig_words.join(" ")
end

def find_switch_point(word)
  vowels = ["a", "e", "i", "o", "u"]
  word.chars.each_with_index do |ch, i|
    return i + 2 if ch == "q" 
    return i if vowels.include?(ch)
  end
end
